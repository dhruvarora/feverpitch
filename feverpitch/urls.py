from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    url(r'', include("articles.urls", namespace="articles")),
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),
    url(r'^newsletter/', include('newsletter.urls', namespace="newsletter")),
    url(r'^football-center/', include("stats.urls", namespace="stats")),
    url(r'^admin/', admin.site.urls),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
