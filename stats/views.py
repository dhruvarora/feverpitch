from django.shortcuts import render
from datetime import datetime, timedelta
from .models import Team, Match, Competition, Player, Season, PointTable, TeamPoint
teams = Team.objects.all().order_by("-date_added")

def index(request):
    return render(request, "index.html", {
    })

def clIndex(request):
    return render(request, "index.html", {
    })

def cupPage(request, cup_id, cup_name):
    return render(request, "index.html", {
    })

def playerPage(request, player_id, player_slug):
    player_id = int(player_id)
    player = Player.objects.get(pk=player_id)
    #print ("player=", player);
    return render(request, "stats/player.html", {
        "player":player,
    })

def playerIndex(request):
    return render(request, "players.html", {
    })

def matchPage(request, match_id, match_slug):
    match=Match.objects.get(pk=match_id);
    #for each in match.home_players_playing.all():
    #    print each.picture.url
    return render(request, "stats/match.html", {
        'match': match,
    })

def teamIndex(request):
    competitions = Competition.objects.all()
    teams = Team.objects.all()
    return render(request, "stats/teams.html", {
    })


def teamPage(request, team_id, team_slug):
    team_id = int(team_id)
    team = Team.objects.get(pk=team_id)
    matches=Match.objects.filter(home_team=team_id)



    return render(request, "stats/team.html", {
        "team":team,

    })


def teamRoster(request, team_id, team_slug):
    team_id = int(team_id)
    team = Team.objects.get(pk=team_id)

    return render(request, "stats/team-roster.html", {
        "team":team,
    })

def teamSchedule(request, team_id, team_slug):
    team_id = int(team_id)
    team = Team.objects.get(pk=team_id)
    matches=Match.objects.all()
    startdate = datetime.today()

    try:
        home_matches = Match.objects.filter(home_team=team_id).filter(date_of_match__gte=startdate)
    except:
        home_matches=None
    try:
        away_matches = Match.objects.filter(away_team=team_id).filter(date_of_match__gte=startdate)
    except:
        away_matches=None


    print("home_matches:", home_matches, "away_matches:", away_matches)

    return render(request, "stats/team-schedule.html", {
    "team":team, "away_matches":away_matches, "home_matches":home_matches,
    })

def teamResult(request, team_id, team_slug):
    team_id = int(team_id)
    team = Team.objects.get(pk=team_id)
    matches=Match.objects.all()
    startdate = datetime.today()

    #print("home_matches:", home_matches, "away_matches:", away_matches)

    try:
        home_matches = Match.objects.filter(home_team=team_id).filter(date_of_match__lt=startdate)
    except:
        home_matches=None
    try:
        away_matches = Match.objects.filter(away_team=team_id).filter(date_of_match__lt=startdate)
    except:
        away_matches=None

    #print("home_matches:", home_matches, "away_matches:", away_matches)

    return render(request, "stats/team-result.html", {
    "team":team, "away_matches":away_matches, "home_matches":home_matches,
    })

def leaguePage(request, league_id, league_slug):

    league_id = int(league_id)
    league = Competition.objects.get(pk=league_id)
    seasons = Season.objects.filter(competiton=league)
    #print "season", seasons

    startdate = datetime.today()

    results=Match.objects.filter(season__in=seasons).filter(date_of_match__lt=startdate)
    fixtures=Match.objects.filter(season__in=seasons).filter(date_of_match__gte=startdate).order_by("-date_of_match")
    #print "results", results, "fixtures", fixtures

    teams = Team.objects.filter(competition=league_id);

    pointTable = PointTable.objects.filter(competition=league_id)
    teamPoint = TeamPoint.objects.filter(competition__in=pointTable)

    return render(request, "stats/league.html", {
        "league":league,
        "results":results,
        "fixtures":fixtures,
        # "teams": teams,
        "teamPoint": teamPoint

    })
