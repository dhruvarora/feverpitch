from .models import Competition, Team
from articles.models import Category

def dropdown_menu_info(context):
    teams = Team.objects.all()
    competitions = Competition.objects.all()
    categories = Category.objects.all()

    return {'teams':teams, 'competitions':competitions, "categories":categories}
