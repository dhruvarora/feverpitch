from django.contrib import admin
from django import forms


from .models import Team, Player, Match, Competition, Season, Venue, TopPlayer, Management, PointTable, TeamPoint

class TeamAdmin(admin.ModelAdmin):
	list_display = ["name", "date_added", "logo", "state"]
	filter_horizontal = ['players', "managers"]

class CompetitionAdmin(admin.ModelAdmin):
    list_display = ["name", "date_added", "league_format", "logo"]

class TopPlayerAdmin(admin.ModelAdmin):
    list_display = ["player", "date_added"]

class MatchAdmin(admin.ModelAdmin):
	filter_horizontal = ["home_players_playing", "away_players_playing", "home_players_substitutes", "away_players_substitutes"]

class TeamPointInline(admin.TabularInline):
	model = TeamPoint
	extras = 3

class PointTableAdmin(admin.ModelAdmin):
	list_display = ["competition", "date_added"]
	inlines = [TeamPointInline]

admin.site.register(Team, TeamAdmin)
admin.site.register(Player)
admin.site.register(Management)
admin.site.register(Match, MatchAdmin)
admin.site.register(Competition, CompetitionAdmin)
admin.site.register(Season)
admin.site.register(Venue)
admin.site.register(TopPlayer, TopPlayerAdmin)
admin.site.register(PointTable, PointTableAdmin)
