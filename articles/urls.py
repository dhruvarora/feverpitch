from django.conf.urls import url
from . import views
from django.views.generic import RedirectView


urlpatterns = [
    url(r'^$', RedirectView.as_view(url='/1', permanent=False)),
    url(r'^(?P<page>\d+)/$', views.index, name="index"),
    #url(r'^$', views.index, name="index"),
    url(r'^category/(?P<pk>\d+)/(?P<slug>.+)/$', views.CategoryView, name="category"),
    url(r'^post/(?P<pk>\d+)/(?P<slug>.+)/$', views.PostView, name="post"),
    url(r'^search/$', views.searchView, name="search"),
    url(r'^database_entry/$', views.database_entry),

]
