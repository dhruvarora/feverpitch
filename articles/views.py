from django.shortcuts import render

from .models import Article, Category
from stats.models import Team, Competition
import json
from django.http import JsonResponse
from django.contrib.auth.models import User
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.views.decorators.csrf import csrf_protect



description = "Fever Pitch is Indias only online Indian Football News and Magazine Website. From Mohan Bagar to Meghalaya FC, we cover all!"
keywords = "feverpitch, fever pitch, fever pitch india, mohan bagan, football india, football in india, indian football team, india fc, football clubs in india"
categories = Category.objects.all()
teams = Team.objects.all().order_by("-date_added")
competitions = Competition.objects.all()


def index(request, page):
    page=int(page)
    print("page=", page)
    page_range= range(page-2, page+3)

    homepage_posts = Article.objects.filter(published=True, featured=True).order_by("-date_added")[:2]
    home_bottom4 = Article.objects.filter(published=True, featured=True).order_by("-date_added")[3:6]
    latest_articles = Article.objects.filter(published=True).order_by("-date_added")[:5]
    #more_articles = Article.objects.filter(published=True).order_by("-date_added")
    popular_news = Article.objects.filter(published=True).order_by("hits")[:10]
    #///////////////////////////////////////////


    more_articles_list = Article.objects.filter(published=True).order_by("-date_added")
    print("page=", page)

    paginator = Paginator(more_articles_list, 4)

    try:
        more_articles = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        more_articles = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        more_articles = paginator.page(paginator.num_pages)

    #///////////////////////////////////////////


    return render(request, "index.html", {
    "homepagePosts":homepage_posts, "homeb4":home_bottom4, "latest_articles":latest_articles,
    "description":description, "keywords":keywords, "articles":more_articles, "popular":popular_news,
    "page_range":page_range,"total_pages": paginator.num_pages,

    })


def CategoryView(request, pk, slug):
    category = Category.objects.get(pk=pk)
    posts = Article.objects.filter(published=True).order_by("-date_added")

    return render(request, "articles/category.html", {
    "description":description, "keywords": keywords, "posts":posts,
    "category":category,
    })

def PostView(request, pk, slug):
    post = Article.objects.get(pk=pk)
    post.hits = post.hits+1
    post.save()

    if not post.descriptionMeta:
        if not post.introText:
            description = "Fever Pitch is India's only online Indian Football News and Magazine Website. From Mohan Bagar to Meghalaya FC, we cover all!"
        else:
            description = post.introText
    else:
        description = post.descriptionMeta

    if not post.keywords:
        keywords = "feverpitch, fever pitch, fever pitch india, mohan bagan, football india, football in india, indian football team, india fc, football clubs in india"
    else:
        keywords = post.keywords

    return render(request, "articles/post.html", {
    "post":post, "description":description, "keywords":keywords,
    })


def searchView(request):
    page = request.GET.get("page")
    page=int(page)
    print("page=", page)
    page_range= range(page-2, page+3)

    term = request.GET.get("term")
    results_list = Article.objects.filter(title__icontains=term)

    paginator = Paginator(results_list, 5)

    try:
        results = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        results = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        results = paginator.page(paginator.num_pages)

    return render(request, "search.html", {
    "results":results,
    "term": term,
    "page_range":page_range,"total_pages": paginator.num_pages,
    "count": len(results_list),
    })

def database_entry(request):
    with open('file_final.json') as data_file:
        data = json.load(data_file)
        print (data)
        for each in data:
        	# print each["id"],each["date_added"], each["author"] , each["intro_text"], each["content"], each["image"], each["category"]
            article = Article(
                date_added = each["date_added"],
                category = Category.objects.get(pk=1), #each["category"], iske setting karni padegi ForeignKey hai ye
                title = each["title"],
                author = User.objects.get(pk=1),  #each["author"], , iske setting karni padegi ForeignKey hai ye
                mainImage = each["image"],
                introText = each["intro_text"],
                introImage = each["image"],
                content = each["content"],
                )
            article.save()




    return JsonResponse({'result':"Entries done to database, check ur admin-article page to see the difference"})
