from __future__ import unicode_literals

from django.db import models
from ckeditor.fields import RichTextField
from django.contrib.auth.models import User
from django.utils import timezone

now = timezone.now()

class Category(models.Model):
    date_added = models.DateField(default=now)
    name = models.CharField(max_length=150)

    def __str__(self):
        return self.name

class Article(models.Model):
    date_added = models.DateField(default=now)
    category = models.ForeignKey(Category)
    published = models.BooleanField(default=True, choices=(
        (True, "Yes"),
        (False, "No")
    ))
    featured = models.BooleanField(default=True, choices=(
        (True, "Yes"),
        (False, "No")
    ))
    title = models.CharField(max_length=150)
    author = models.ForeignKey(User)
    introImage = models.FileField(upload_to="articles/intro-images", max_length=150)
    introText = models.TextField()
    mainImage = models.FileField(upload_to="articles/main-images", max_length=150, blank=True, null=True)
    content = RichTextField(blank=True, null=True)
    keywords = models.CharField(max_length=250, blank=True, null=True)
    descriptionMeta = models.TextField(blank=True, null=True)
    hits = models.IntegerField(default=0)

    def __str__(self):
        return self.title
