from django.contrib import admin

from .models import Category, Article

class ArticleAdmin(admin.ModelAdmin):
    list_display = ["title", "date_added", "category", "author", "hits", "published"]
    list_filter = ["category", "hits", "author"]
    search_fields = ["title", "category__name", "author__first_name"]
    list_editable = ["published"]
    readonly_fields = ["hits"]

admin.site.register(Category)
admin.site.register(Article, ArticleAdmin)
